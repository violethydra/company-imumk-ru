/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./development/components/js/connect.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./development/components/js/connect.js":
/*!**********************************************!*\
  !*** ./development/components/js/connect.js ***!
  \**********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modules/moveScrollUP */ "./development/components/js/modules/moveScrollUP.js");
/* harmony import */ var _modules_openMyBurger__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modules/openMyBurger */ "./development/components/js/modules/openMyBurger.js");
/* harmony import */ var _modules_AddServerData__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modules/AddServerData */ "./development/components/js/modules/AddServerData.js");
// ============================
//    Name: index.js
// ============================




var start = function start() {
  console.log('DOM:', 'DOMContentLoaded', true);
  new _modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_0__["default"]({
    selector: '.js__moveScrollUP',
    speed: 8
  }).run();
  new _modules_openMyBurger__WEBPACK_IMPORTED_MODULE_1__["default"]({
    burger: '.js__navHamburger',
    navbar: '.js__navHamburgerOpener',
    list: '.js__navHamburgerClone'
  }).run();
  new _modules_AddServerData__WEBPACK_IMPORTED_MODULE_2__["default"]({
    selector: '.js__serverControlPanel',
    dataUrl: 'http://krapipl.imumk.ru:8082/api/mobilev1/update'
  }).run();
};

if (typeof window !== 'undefined' && window && window.addEventListener) {
  document.addEventListener('DOMContentLoaded', start(), false);
}

/***/ }),

/***/ "./development/components/js/modules/AddCardGenerator.js":
/*!***************************************************************!*\
  !*** ./development/components/js/modules/AddCardGenerator.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return AddCardGenerator; });
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var AddCardGenerator =
/*#__PURE__*/
function () {
  function AddCardGenerator(init) {
    _classCallCheck(this, AddCardGenerator);

    this.selector = document.querySelector('.js__stuffGenerator');
    this.picture = init.picture || '__NULL__';
    this.title = init.title || '__NULL__';
    this.grade = init.grade || '__NULL__';
    this.genre = init.genre || '__NULL__';
    this.extra = init.extra || '__NULL__';
    this.button = init.button || '__NULL__';
    this.extraName = 'класс';
    this.constructor.info();
    this.buildCard();

    this._handler();
  }

  _createClass(AddCardGenerator, [{
    key: "gradeFix",
    value: function gradeFix() {
      var _this = this;

      var singleNumber = function singleNumber(x) {
        return "".concat(x[0], " ").concat(_this.extraName);
      };

      var manyNumber = function manyNumber(x) {
        return "".concat(Math.min.apply(Math, _toConsumableArray(x)), "-").concat(Math.max.apply(Math, _toConsumableArray(x)), " ").concat(_this.extraName, "\u044B");
      };

      var result = function result(arr) {
        return arr.length === 1 ? singleNumber(arr) : manyNumber(arr);
      };

      var stringToNumber = function stringToNumber(arr) {
        return arr.split(';').map(Number);
      };

      return result(stringToNumber(this.grade));
    }
  }, {
    key: "buildCard",
    value: function buildCard() {
      var createDiv = document.createElement('DIV');
      createDiv.classList.add('startpage__stuff-item');
      createDiv.innerHTML += " \n\t\t\t\t<div class=\"stuffcard stuffcard__box\">\n\t\t\t\t\t<div class=\"stuffcard__image\">\n\t\t\t\t\t\t<img src=\"https://www.imumk.ru/svc/coursecover/".concat(this.picture, "\" alt=\"Picture\" title=\"Picture\">\n\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"stuffcard__desc\">\n\t\t\t\t\t\t\t<div class=\"stuffcard__subject\">").concat(this.title, "</div>\n\t\t\t\t\t\t\t<div class=\"stuffcard__grade\">").concat(this.gradeFix(), "</div>\n\t\t\t\t\t\t\t<div class=\"stuffcard__genre\">").concat(this.genre, "</div>\n\t\t\t\t\t\t\t<div class=\"stuffcard__extra\"><a class=\"stuffcard__href\" title=\"URL\" role=\"link\" href=\"").concat(this.extra, "\">\u041F\u043E\u0434\u0440\u043E\u0431\u043D\u0435\u0435</a></div>\n\t\t\t\t\t\t\t<a class=\"stuffcard__button\" title=\"URL\" role=\"link\" href=\"").concat(this.button, "\">\u041F\u043E\u043F\u0440\u043E\u0431\u043E\u0432\u0430\u0442\u044C</a>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div> \n\t\t\t\t");
      this.selector.appendChild(createDiv);
    }
  }, {
    key: "whatClicked",
    value: function whatClicked(event) {
      this._target = event.target;
      event.preventDefault();
      if (this._target.matches('A')) console.log('click :', this._target.href);
    }
  }, {
    key: "_handler",
    value: function _handler() {
      this.selector.addEventListener('click', this.whatClicked, false);
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('•• ADDON:', this.name, true);
    }
  }]);

  return AddCardGenerator;
}();



/***/ }),

/***/ "./development/components/js/modules/AddCategoryGenerator.js":
/*!*******************************************************************!*\
  !*** ./development/components/js/modules/AddCategoryGenerator.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return AddCategoryGenerator; });
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var AddCategoryGenerator =
/*#__PURE__*/
function () {
  function AddCategoryGenerator(init) {
    _classCallCheck(this, AddCategoryGenerator);

    this.selector = init.selector;
    this.arr = init.array;
    this.category = init.category;
    this.tooltip = init.tooltip || '__NULL__';
    this.myClassName = 'dropdown__item dropdown__select';
    this.selectLi = document.querySelector('.js__createDropList');

    this.createParent = function () {
      return document.createElement('UL');
    };

    this.createChild = function () {
      return document.createElement('LI');
    };

    this.constructor.info();
    this.categoryBuildList();
  }

  _createClass(AddCategoryGenerator, [{
    key: "categoryBuildList",
    value: function categoryBuildList() {
      var _this = this;

      var createUL = this.createParent();
      createUL.className = this.myClassName;
      var createLI = this.createChild();
      createLI.innerHTML = this.tooltip;
      createUL.appendChild(createLI);
      var nextStep = this.category !== 'grade' ? this.calculateList() : this.calculateNumbers();
      nextStep.forEach(function (f) {
        createLI = _this.createChild();
        createLI.innerHTML += f;
        createUL.appendChild(createLI);
      });
      this.selectLi.querySelector(this.selector).parentElement.appendChild(createUL);
    }
  }, {
    key: "calculateList",
    value: function calculateList() {
      var _this2 = this;

      return _toConsumableArray(new Set(this.arr.map(function (f) {
        return f[_this2.category];
      }))).sort();
    }
  }, {
    key: "calculateNumbers",
    value: function calculateNumbers() {
      var myNumber = this.calculateList().map(Number).reduce(function (acc, val) {
        acc[0] = acc[0] === undefined || val < acc[0] ? val : acc[0];
        acc[1] = acc[1] === undefined || val > acc[1] ? val : acc[1];
        return acc;
      }, []);
      return this.calculateRange.apply(this, _toConsumableArray(myNumber));
    }
  }, {
    key: "calculateRange",
    value: function calculateRange(start, end) {
      this.result = _toConsumableArray(Array(1 + end - start).keys()).map(function (f) {
        return f + start;
      });
      return this.result;
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('•• ADDON:', this.name, true);
    }
  }]);

  return AddCategoryGenerator;
}();



/***/ }),

/***/ "./development/components/js/modules/AddServerData.js":
/*!************************************************************!*\
  !*** ./development/components/js/modules/AddServerData.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return AddServerData; });
/* harmony import */ var _AddCardGenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AddCardGenerator */ "./development/components/js/modules/AddCardGenerator.js");
/* harmony import */ var _AddCategoryGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AddCategoryGenerator */ "./development/components/js/modules/AddCategoryGenerator.js");
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }




var AddServerData =
/*#__PURE__*/
function () {
  function AddServerData(init) {
    _classCallCheck(this, AddServerData);

    this.selector = document.querySelector(init.selector);
    this.dataUrl = init.dataUrl;
    this.loader = document.querySelector('.loader__box');
    this.mainSelector = document.querySelector('MAIN');
    this.inputSearch = document.querySelector('input[name=search]');
    this.clickInput = document.querySelector('.js__clickEnter');
    this.selectorNothing = document.querySelector('.js__resultNothing');
    this.cardTouch = false;
    this.clickedTarget = '';
    this.clickedTargetName = '';
    this.removeLoopClick = '';
    this.hiddenUrl = '';
    this.filterFlag = Boolean(false);
    this.errorText = '💀 Ouch, database error...';
  }

  _createClass(AddServerData, [{
    key: "run",
    value: function run() {
      if (this.selector) {
        this.constructor.info();
        this.fetchMyData();
      }
    }
  }, {
    key: "successLoading",
    value: function successLoading() {
      var _this = this;

      var listiner = function listiner() {
        return _this.loader.classList.toggle('hidden');
      };

      setTimeout(listiner, 100);
    }
  }, {
    key: "eventHandlers",
    value: function eventHandlers() {
      var _this2 = this;

      this.mainSelector.addEventListener('click', function (event) {
        return _this2.closeAndOpenFilter(event);
      }, false);
      this.inputSearch.addEventListener('input', function (event) {
        return _this2.liveSearch(event);
      }, false);
      this.inputSearch.addEventListener('keydown', function (event) {
        return _this2.keyboardEnter(event);
      }, false);
      this.clickInput.addEventListener('click', function (event) {
        return _this2.keyboardEnterClick(event);
      }, false);
    }
  }, {
    key: "__cleanSearchResult",
    value: function __cleanSearchResult() {
      this.link = _toConsumableArray(document.querySelectorAll('.startpage__stuff-item.load'));
      this.render = this.link.filter(function (f) {
        return f.classList.remove('load');
      });
    }
  }, {
    key: "_resultEnter",
    value: function _resultEnter() {
      var _this3 = this;

      if (this.removeLoopClick === this.enterText) return;

      this.__cleanSearchResult();

      this.link = _toConsumableArray(document.querySelectorAll('.stuffcard__button'));
      this.result = this.liveSearchResult || [''];
      this.totalResult = this.link.filter(function (f) {
        return _this3.result.some(function (s) {
          return Number(f.href.split('#')[1]) === s;
        });
      });
      this.totalResult.map(function (f) {
        return f.closest('.startpage__stuff-item').classList.add('load');
      });
      this.removeLoopClick = this.enterText;

      this._renderZeroResult();
    }
  }, {
    key: "keyboardEnterClick",
    value: function keyboardEnterClick(event) {
      this._resultEnter();
    }
  }, {
    key: "keyboardEnter",
    value: function keyboardEnter(event) {
      this.enterCode = 'Enter';
      if (event.key === this.enterCode) this._resultEnter();
    }
  }, {
    key: "liveSearch",
    value: function liveSearch() {
      var _this4 = this;

      this.enterText = String(this.inputSearch.value).trim();
      this.regMagic = /\s/g;
      this.splitEnterText = this.enterText.toLowerCase().split(this.regMagic).map(String);

      this.textFilter = function (f) {
        return _this4.splitEnterText.every(function (s) {
          return f.title.includes(s);
        });
      };

      this.liveSearchResult = this.mainDataLower.filter(this.textFilter).map(function (f) {
        return Number(f.courseId);
      });
    }
  }, {
    key: "closeAndOpenFilter",
    value: function closeAndOpenFilter(event) {
      this._target = event.target;

      if (this._target.id !== this.removeLoopClick) {
        this.mainSelector.querySelectorAll('.js__createDropList UL.open').forEach(function (f) {
          return f.classList.remove('open');
        });
      }

      if (this._target.matches('.dropdown__label')) {
        event.preventDefault();
        event.stopPropagation();
      }

      if (this._target.matches('INPUT[type=button]')) {
        this._target.parentElement.querySelector('UL').classList.add('open');

        this.removeLoopClick = this._target.id;
        this.clickedTargetName = this._target.parentElement.querySelector('INPUT').name;
        this.hiddenUrl = this._target.value;
      }

      if (this._target.nodeName === 'LI') {
        this.grabText = this._target.textContent;
        this.clickedTarget = this.grabText;
        if (this.hiddenUrl === this.grabText) return;
        this.pasteInSelector = this._target.parentElement.parentElement;
        this.pasteInSelector.querySelector('INPUT[type=button]').value = this.grabText || '';

        this._closeAndOpenSaveResult();

        this._renderResultAlt();
      }
    }
  }, {
    key: "_closeAndOpenSaveResult",
    value: function _closeAndOpenSaveResult() {
      this.defaultInputValue = _toConsumableArray(document.querySelectorAll('INPUT[type=button]')).map(function (f) {
        return f.value;
      });
    }
  }, {
    key: "__renderResultGrade",
    value: function __renderResultGrade() {
      var _this5 = this;

      this.result = this.mainData.filter(function (f) {
        return f.grade.split(';').includes(_this5._target);
      }).map(function (f) {
        return Number(f.courseId);
      });
      this.link = _toConsumableArray(document.querySelectorAll('.stuffcard__button'));
      this.gradeResult = this.link.filter(function (f) {
        return _this5.result.some(function (s) {
          return Number(f.href.split('#')[1]) === s;
        });
      });
      this.gradeResult.map(function (f) {
        return f.closest('.startpage__stuff-item').classList.add('load');
      });
    }
  }, {
    key: "__renderResultSubject",
    value: function __renderResultSubject() {
      var _this6 = this;

      this.link = _toConsumableArray(document.querySelectorAll(".stuffcard__".concat(this.clickedTargetName)));
      this.subjectResult = this.link.filter(function (f) {
        return f.textContent.includes(_this6._target);
      });
      this.subjectResult.map(function (f) {
        return f.closest('.startpage__stuff-item').classList.add('load');
      });
    }
  }, {
    key: "_renderResultAlt",
    value: function _renderResultAlt() {
      var _this7 = this;

      this.__cleanSearchResult();

      this.passedResult = this.defaultInputValue.map(function (f, index) {
        return _this7.defaultInputValue[index] === _this7.linkDefaultValue[index] ? '' : _this7.defaultInputValue[index];
      });

      this.changeResultAfterClick = function (target, number) {
        if (_this7.defaultInputValue[number] === _this7.linkDefaultValue[number]) return _this7.defaultInputValue[number];
        if (number === 2) return target[_this7.defaultInputName[number]].split(';').includes(_this7.passedResult[number]);
        return target[_this7.defaultInputName[number]].includes(_this7.passedResult[number]);
      };

      this.result = this.mainData.filter(function (f) {
        return _this7.changeResultAfterClick(f, 0);
      }).filter(function (f) {
        return _this7.changeResultAfterClick(f, 1);
      }).filter(function (f) {
        return _this7.changeResultAfterClick(f, 2);
      }).map(function (f) {
        return Number(f.courseId);
      });
      this.link = _toConsumableArray(document.querySelectorAll('.stuffcard__button'));
      this.totalResult = this.link.filter(function (f) {
        return _this7.result.some(function (s) {
          return Number(f.href.split('#')[1]) === s;
        });
      });
      this.totalResult.map(function (f) {
        return f.closest('.startpage__stuff-item').classList.add('load');
      });

      this._renderZeroResult();
    }
  }, {
    key: "_renderZeroResult",
    value: function _renderZeroResult() {
      if (this.result.length <= 0) this.selectorNothing.classList.add('show');else this.selectorNothing.classList.remove('show');
    }
  }, {
    key: "renderCards",
    value: function renderCards() {
      this.link = _toConsumableArray(document.querySelectorAll('.startpage__stuff-item'));
      this.render = this.link.filter(function (f) {
        return f.classList.add('load');
      });
    }
  }, {
    key: "afterBuildingCards",
    value: function afterBuildingCards() {
      this.renderCards();
      this.linkDefaultValue = _toConsumableArray(document.querySelectorAll('[type=button]')).map(function (f) {
        return f.value;
      });
      this.defaultInputName = _toConsumableArray(document.querySelectorAll('INPUT[type=button]')).map(function (f) {
        return f.name;
      });
      this.liveSearch();
    }
  }, {
    key: "buildFilterList",
    value: function buildFilterList() {
      var _this8 = this;

      var selectAllFilters = _toConsumableArray(document.querySelectorAll('input[type=button]'));

      this.categoryCleanData = selectAllFilters.map(function (f, index) {
        return {
          selectorID: selectAllFilters[index].id,
          selectorName: selectAllFilters[index].name,
          selectorValue: selectAllFilters[index].value
        };
      });

      var newCategory = function newCategory(x) {
        return new _AddCategoryGenerator__WEBPACK_IMPORTED_MODULE_1__["default"](x);
      };

      this.categoryCleanData.map(function (f, index) {
        return newCategory({
          selector: "#".concat(f.selectorID),
          array: _this8.mainData,
          category: f.selectorName,
          tooltip: f.selectorValue
        });
      });
    }
  }, {
    key: "fetchMyData",
    value: function fetchMyData() {
      var _this9 = this;

      var sendRequest = JSON.stringify({
        data: ''
      }, null, '\t');
      var options = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        mode: 'cors',
        cache: 'default',
        body: sendRequest
      };
      var originUrl = this.dataUrl;
      var noCORS = 'https://cors-anywhere.herokuapp.com/';
      var url = "".concat(noCORS).concat(originUrl); // const url = `${originUrl}`;

      fetch(url, options).then(function (response) {
        return response.json();
      }).then(function (data) {
        _this9.mainData = _toConsumableArray(data.items);
        _this9.mainDataLower = _this9.mainData.map(function (f) {
          return _objectSpread({}, f, {
            title: f.title.toLowerCase()
          });
        });

        var newCard = function newCard(x) {
          return new _AddCardGenerator__WEBPACK_IMPORTED_MODULE_0__["default"](x);
        };

        _this9.mainData.map(function (f) {
          return newCard({
            selector: _this9.selector,
            picture: f.courseId,
            title: f.subject,
            grade: f.grade,
            genre: f.genre,
            extra: "#".concat(f.shopUrl),
            button: "#".concat(f.courseId)
          });
        });
      }).then(this.buildFilterList.bind(this)).then(this.afterBuildingCards.bind(this)).then(this.eventHandlers.bind(this)).then(this.successLoading.bind(this)); // .catch(onRejected => console.error(this.errorText, `\n${onRejected.message}`));
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('• MODULE:', this.name, true);
    }
  }]);

  return AddServerData;
}();



/***/ }),

/***/ "./development/components/js/modules/moveScrollUP.js":
/*!***********************************************************!*\
  !*** ./development/components/js/modules/moveScrollUP.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return AddMoveScrollUP; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var AddMoveScrollUP =
/*#__PURE__*/
function () {
  function AddMoveScrollUP(init) {
    _classCallCheck(this, AddMoveScrollUP);

    this.selector = document.querySelector(init.selector);
    this.count = Number(Math.abs(init.speed)) || 8;
    this.speed = this.count <= 20 ? this.count : 8;
    this.myPos = window.pageYOffset;
    this.getScroll = 0;
    this.speedScroll = this.speed;
  }

  _createClass(AddMoveScrollUP, [{
    key: "scrollUpSetting",
    value: function scrollUpSetting() {
      var _this = this;

      var clickedArrow = function clickedArrow() {
        _this.getScroll = document.documentElement.scrollTop;

        if (_this.getScroll >= 1) {
          window.requestAnimationFrame(clickedArrow);
          window.scrollTo(0, _this.getScroll - _this.getScroll / _this.speedScroll);
        }
      };

      var scrolledDown = function scrolledDown() {
        _this.myPos = window.pageYOffset;
        _this.myPos >= 100 ? _this.selector.classList.add('show') : _this.selector.classList.remove('show');
      };

      this.selector.addEventListener('click', clickedArrow, false);
      window.addEventListener('scroll', scrolledDown, false);
    }
  }, {
    key: "run",
    value: function run() {
      if (this.selector) {
        this.constructor.info();
        this.scrollUpSetting();
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('• MODULE:', this.name, true);
    }
  }]);

  return AddMoveScrollUP;
}();



/***/ }),

/***/ "./development/components/js/modules/openMyBurger.js":
/*!***********************************************************!*\
  !*** ./development/components/js/modules/openMyBurger.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return AddOpenMyBurger; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var AddOpenMyBurger =
/*#__PURE__*/
function () {
  function AddOpenMyBurger(init) {
    _classCallCheck(this, AddOpenMyBurger);

    this.selector = document.querySelector(init.burger);
    this.navbar = document.querySelector(init.navbar);
    this.cloneMe = document.querySelector(init.list);
    this.errorText = '💀 Ouch, database error...';
  }

  _createClass(AddOpenMyBurger, [{
    key: "run",
    value: function run() {
      if (this.selector) {
        this.constructor.info();
        this.createCopyLinks();
        this.eventHandlers();
      }
    }
  }, {
    key: "eventHandlers",
    value: function eventHandlers() {
      var _this = this;

      this.selector.addEventListener('click', function (event) {
        return _this.toggleBurger(event);
      }, false);
    }
  }, {
    key: "createCopyLinks",
    value: function createCopyLinks() {
      try {
        this.newCloneDiv = document.importNode(this.navbar, true);
        this.newCloneDiv.classList.add('myheader__list--cloned');
        this.newCloneDiv.classList.remove('myheader__list');
        this.cloneMe.after(this.newCloneDiv);
      } catch (error) {
        console.error('[ERROR] Hamburger list is empty!');
      }
    }
  }, {
    key: "toggleBurger",
    value: function toggleBurger() {
      this.selector.classList.toggle('open');
      if (window.innerWidth <= 678) this.navbar.classList.toggle('open');
      if (window.innerWidth >= 678) this.newCloneDiv.classList.toggle('open');
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('• MODULE:', this.name, true);
    }
  }]);

  return AddOpenMyBurger;
}();



/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29ubmVjdC5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly8vLi9kZXZlbG9wbWVudC9jb21wb25lbnRzL2pzL2Nvbm5lY3QuanMiLCJ3ZWJwYWNrOi8vLy4vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL0FkZENhcmRHZW5lcmF0b3IuanMiLCJ3ZWJwYWNrOi8vLy4vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL0FkZENhdGVnb3J5R2VuZXJhdG9yLmpzIiwid2VicGFjazovLy8uL2RldmVsb3BtZW50L2NvbXBvbmVudHMvanMvbW9kdWxlcy9BZGRTZXJ2ZXJEYXRhLmpzIiwid2VicGFjazovLy8uL2RldmVsb3BtZW50L2NvbXBvbmVudHMvanMvbW9kdWxlcy9tb3ZlU2Nyb2xsVVAuanMiLCJ3ZWJwYWNrOi8vLy4vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL29wZW5NeUJ1cmdlci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL2RldmVsb3BtZW50L2NvbXBvbmVudHMvanMvY29ubmVjdC5qc1wiKTtcbiIsIi8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT1cclxuLy8gICAgTmFtZTogaW5kZXguanNcclxuLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PVxyXG5cclxuaW1wb3J0IEFkZE1vdmVTY3JvbGxVUCBmcm9tICcuL21vZHVsZXMvbW92ZVNjcm9sbFVQJztcclxuaW1wb3J0IEFkZE9wZW5NeUJ1cmdlciBmcm9tICcuL21vZHVsZXMvb3Blbk15QnVyZ2VyJztcclxuaW1wb3J0IEFkZFNlcnZlckRhdGEgZnJvbSAnLi9tb2R1bGVzL0FkZFNlcnZlckRhdGEnO1xyXG5cclxuY29uc3Qgc3RhcnQgPSAoKSA9PiB7XHJcblx0Y29uc29sZS5sb2coJ0RPTTonLCAnRE9NQ29udGVudExvYWRlZCcsIHRydWUpO1xyXG5cclxuXHRuZXcgQWRkTW92ZVNjcm9sbFVQKHtcclxuXHRcdHNlbGVjdG9yOiAnLmpzX19tb3ZlU2Nyb2xsVVAnLFxyXG5cdFx0c3BlZWQ6IDhcclxuXHR9KS5ydW4oKTtcclxuXHJcblx0bmV3IEFkZE9wZW5NeUJ1cmdlcih7XHJcblx0XHRidXJnZXI6ICcuanNfX25hdkhhbWJ1cmdlcicsXHJcblx0XHRuYXZiYXI6ICcuanNfX25hdkhhbWJ1cmdlck9wZW5lcicsXHJcblx0XHRsaXN0OiAnLmpzX19uYXZIYW1idXJnZXJDbG9uZSdcclxuXHR9KS5ydW4oKTtcclxuXHJcblx0bmV3IEFkZFNlcnZlckRhdGEoe1xyXG5cdFx0c2VsZWN0b3I6ICcuanNfX3NlcnZlckNvbnRyb2xQYW5lbCcsXHJcblx0XHRkYXRhVXJsOiAnaHR0cDovL2tyYXBpcGwuaW11bWsucnU6ODA4Mi9hcGkvbW9iaWxldjEvdXBkYXRlJ1xyXG5cdH0pLnJ1bigpO1xyXG5cclxufTtcclxuXHJcbmlmICh0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJiB3aW5kb3cgJiYgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIpIHtcclxuXHRkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdET01Db250ZW50TG9hZGVkJywgc3RhcnQoKSwgZmFsc2UpO1xyXG59IiwiZXhwb3J0IGRlZmF1bHQgY2xhc3MgQWRkQ2FyZEdlbmVyYXRvciB7XHJcblx0Y29uc3RydWN0b3IoaW5pdCkge1xyXG5cdFx0dGhpcy5zZWxlY3RvciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5qc19fc3R1ZmZHZW5lcmF0b3InKTtcclxuXHRcdHRoaXMucGljdHVyZSA9IGluaXQucGljdHVyZSB8fCAnX19OVUxMX18nO1xyXG5cdFx0dGhpcy50aXRsZSA9IGluaXQudGl0bGUgfHwgJ19fTlVMTF9fJztcclxuXHRcdHRoaXMuZ3JhZGUgPSBpbml0LmdyYWRlIHx8ICdfX05VTExfXyc7XHJcblx0XHR0aGlzLmdlbnJlID0gaW5pdC5nZW5yZSB8fCAnX19OVUxMX18nO1xyXG5cdFx0dGhpcy5leHRyYSA9IGluaXQuZXh0cmEgfHwgJ19fTlVMTF9fJztcclxuXHRcdHRoaXMuYnV0dG9uID0gaW5pdC5idXR0b24gfHwgJ19fTlVMTF9fJztcclxuXHRcdHRoaXMuZXh0cmFOYW1lID0gJ9C60LvQsNGB0YEnO1xyXG5cclxuXHRcdHRoaXMuY29uc3RydWN0b3IuaW5mbygpO1xyXG5cdFx0dGhpcy5idWlsZENhcmQoKTtcclxuXHRcdHRoaXMuX2hhbmRsZXIoKTtcclxuXHR9XHJcblxyXG5cdHN0YXRpYyBpbmZvKCkgeyBjb25zb2xlLmxvZygn4oCi4oCiIEFERE9OOicsIHRoaXMubmFtZSwgdHJ1ZSk7IH1cclxuXHRcclxuXHRncmFkZUZpeCgpIHtcclxuXHRcdGNvbnN0IHNpbmdsZU51bWJlciA9IHggPT4gYCR7eFswXX0gJHt0aGlzLmV4dHJhTmFtZX1gO1xyXG5cdFx0Y29uc3QgbWFueU51bWJlciA9IHggPT4gYCR7TWF0aC5taW4oLi4ueCl9LSR7TWF0aC5tYXgoLi4ueCl9ICR7dGhpcy5leHRyYU5hbWV90YtgO1xyXG5cdFx0Y29uc3QgcmVzdWx0ID0gYXJyID0+IChhcnIubGVuZ3RoID09PSAxID8gc2luZ2xlTnVtYmVyKGFycikgOiBtYW55TnVtYmVyKGFycikpO1xyXG5cdFx0Y29uc3Qgc3RyaW5nVG9OdW1iZXIgPSBhcnIgPT4gYXJyLnNwbGl0KCc7JykubWFwKE51bWJlcik7XHJcblx0XHRcclxuXHRcdHJldHVybiByZXN1bHQoc3RyaW5nVG9OdW1iZXIodGhpcy5ncmFkZSkpO1xyXG5cdH1cclxuXHJcblx0YnVpbGRDYXJkKCkgeyBcclxuXHRcdGNvbnN0IGNyZWF0ZURpdiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ0RJVicpO1xyXG5cdFx0Y3JlYXRlRGl2LmNsYXNzTGlzdC5hZGQoJ3N0YXJ0cGFnZV9fc3R1ZmYtaXRlbScpO1xyXG5cdFx0Y3JlYXRlRGl2LmlubmVySFRNTCArPSBgIFxyXG5cdFx0XHRcdDxkaXYgY2xhc3M9XCJzdHVmZmNhcmQgc3R1ZmZjYXJkX19ib3hcIj5cclxuXHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJzdHVmZmNhcmRfX2ltYWdlXCI+XHJcblx0XHRcdFx0XHRcdDxpbWcgc3JjPVwiaHR0cHM6Ly93d3cuaW11bWsucnUvc3ZjL2NvdXJzZWNvdmVyLyR7dGhpcy5waWN0dXJlfVwiIGFsdD1cIlBpY3R1cmVcIiB0aXRsZT1cIlBpY3R1cmVcIj5cclxuXHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwic3R1ZmZjYXJkX19kZXNjXCI+XHJcblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzcz1cInN0dWZmY2FyZF9fc3ViamVjdFwiPiR7dGhpcy50aXRsZX08L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwic3R1ZmZjYXJkX19ncmFkZVwiPiR7dGhpcy5ncmFkZUZpeCgpfTwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJzdHVmZmNhcmRfX2dlbnJlXCI+JHt0aGlzLmdlbnJlfTwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3M9XCJzdHVmZmNhcmRfX2V4dHJhXCI+PGEgY2xhc3M9XCJzdHVmZmNhcmRfX2hyZWZcIiB0aXRsZT1cIlVSTFwiIHJvbGU9XCJsaW5rXCIgaHJlZj1cIiR7dGhpcy5leHRyYX1cIj7Qn9C+0LTRgNC+0LHQvdC10LU8L2E+PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0PGEgY2xhc3M9XCJzdHVmZmNhcmRfX2J1dHRvblwiIHRpdGxlPVwiVVJMXCIgcm9sZT1cImxpbmtcIiBocmVmPVwiJHt0aGlzLmJ1dHRvbn1cIj7Qn9C+0L/RgNC+0LHQvtCy0LDRgtGMPC9hPlxyXG5cdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdDwvZGl2PiBcclxuXHRcdFx0XHRgO1xyXG5cclxuXHRcdHRoaXMuc2VsZWN0b3IuYXBwZW5kQ2hpbGQoY3JlYXRlRGl2KTtcclxuXHR9XHJcblx0XHJcblx0d2hhdENsaWNrZWQoZXZlbnQpIHtcclxuXHRcdHRoaXMuX3RhcmdldCA9IGV2ZW50LnRhcmdldDtcclxuXHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcblx0XHRpZiAodGhpcy5fdGFyZ2V0Lm1hdGNoZXMoJ0EnKSkgY29uc29sZS5sb2coJ2NsaWNrIDonLCB0aGlzLl90YXJnZXQuaHJlZik7XHJcblx0fVxyXG5cdFxyXG5cdF9oYW5kbGVyKCkgeyBcdFxyXG5cdFx0dGhpcy5zZWxlY3Rvci5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIHRoaXMud2hhdENsaWNrZWQsIGZhbHNlKTtcclxuXHR9XHJcbn1cclxuIiwiZXhwb3J0IGRlZmF1bHQgY2xhc3MgQWRkQ2F0ZWdvcnlHZW5lcmF0b3Ige1xyXG5cdGNvbnN0cnVjdG9yKGluaXQpIHtcclxuXHRcdHRoaXMuc2VsZWN0b3IgPSBpbml0LnNlbGVjdG9yO1xyXG5cdFx0dGhpcy5hcnIgPSBpbml0LmFycmF5O1xyXG5cdFx0dGhpcy5jYXRlZ29yeSA9IGluaXQuY2F0ZWdvcnk7XHJcblx0XHR0aGlzLnRvb2x0aXAgPSBpbml0LnRvb2x0aXAgfHwgJ19fTlVMTF9fJztcclxuXHRcdHRoaXMubXlDbGFzc05hbWUgPSAnZHJvcGRvd25fX2l0ZW0gZHJvcGRvd25fX3NlbGVjdCc7XHJcblx0XHR0aGlzLnNlbGVjdExpID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmpzX19jcmVhdGVEcm9wTGlzdCcpO1xyXG5cdFx0dGhpcy5jcmVhdGVQYXJlbnQgPSAoKSA9PiBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdVTCcpO1xyXG5cdFx0dGhpcy5jcmVhdGVDaGlsZCA9ICgpID0+IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ0xJJyk7XHJcblxyXG5cdFx0dGhpcy5jb25zdHJ1Y3Rvci5pbmZvKCk7XHJcblx0XHR0aGlzLmNhdGVnb3J5QnVpbGRMaXN0KCk7XHJcblx0fVxyXG5cclxuXHRzdGF0aWMgaW5mbygpIHsgY29uc29sZS5sb2coJ+KAouKAoiBBRERPTjonLCB0aGlzLm5hbWUsIHRydWUpOyB9XHJcblxyXG5cdGNhdGVnb3J5QnVpbGRMaXN0KCkge1xyXG5cdFx0Y29uc3QgY3JlYXRlVUwgPSB0aGlzLmNyZWF0ZVBhcmVudCgpO1xyXG5cdFx0Y3JlYXRlVUwuY2xhc3NOYW1lID0gdGhpcy5teUNsYXNzTmFtZTtcclxuXHRcdGxldCBjcmVhdGVMSSA9IHRoaXMuY3JlYXRlQ2hpbGQoKTtcclxuXHRcdGNyZWF0ZUxJLmlubmVySFRNTCA9IHRoaXMudG9vbHRpcDtcclxuXHRcdGNyZWF0ZVVMLmFwcGVuZENoaWxkKGNyZWF0ZUxJKTtcclxuXHJcblx0XHRjb25zdCBuZXh0U3RlcCA9IHRoaXMuY2F0ZWdvcnkgIT09ICdncmFkZScgPyB0aGlzLmNhbGN1bGF0ZUxpc3QoKSA6IHRoaXMuY2FsY3VsYXRlTnVtYmVycygpO1xyXG5cdFx0bmV4dFN0ZXAuZm9yRWFjaCgoZikgPT4ge1xyXG5cdFx0XHRjcmVhdGVMSSA9IHRoaXMuY3JlYXRlQ2hpbGQoKTtcclxuXHRcdFx0Y3JlYXRlTEkuaW5uZXJIVE1MICs9IGY7XHJcblx0XHRcdGNyZWF0ZVVMLmFwcGVuZENoaWxkKGNyZWF0ZUxJKTtcclxuXHRcdH0pO1xyXG5cclxuXHRcdHRoaXMuc2VsZWN0TGkucXVlcnlTZWxlY3Rvcih0aGlzLnNlbGVjdG9yKS5wYXJlbnRFbGVtZW50LmFwcGVuZENoaWxkKGNyZWF0ZVVMKTtcclxuXHR9XHJcblxyXG5cdGNhbGN1bGF0ZUxpc3QoKSB7XHJcblx0XHRyZXR1cm4gWy4uLm5ldyBTZXQodGhpcy5hcnIubWFwKGYgPT4gZlt0aGlzLmNhdGVnb3J5XSkpXS5zb3J0KCk7XHJcblx0fVxyXG5cclxuXHRjYWxjdWxhdGVOdW1iZXJzKCkge1xyXG5cdFx0Y29uc3QgbXlOdW1iZXIgPSB0aGlzLmNhbGN1bGF0ZUxpc3QoKS5tYXAoTnVtYmVyKS5yZWR1Y2UoKGFjYywgdmFsKSA9PiB7XHJcblx0XHRcdGFjY1swXSA9IChhY2NbMF0gPT09IHVuZGVmaW5lZCB8fCB2YWwgPCBhY2NbMF0pID8gdmFsIDogYWNjWzBdO1xyXG5cdFx0XHRhY2NbMV0gPSAoYWNjWzFdID09PSB1bmRlZmluZWQgfHwgdmFsID4gYWNjWzFdKSA/IHZhbCA6IGFjY1sxXTtcclxuXHRcdFx0cmV0dXJuIGFjYztcclxuXHRcdH0sIFtdKTtcclxuXHRcdHJldHVybiB0aGlzLmNhbGN1bGF0ZVJhbmdlKC4uLm15TnVtYmVyKTtcclxuXHR9XHJcblxyXG5cdGNhbGN1bGF0ZVJhbmdlKHN0YXJ0LCBlbmQpIHtcclxuXHRcdHRoaXMucmVzdWx0ID0gWy4uLkFycmF5KDEgKyBlbmQgLSBzdGFydCkua2V5cygpXS5tYXAoZiA9PiBmICsgc3RhcnQpO1xyXG5cdFx0cmV0dXJuIHRoaXMucmVzdWx0O1xyXG5cdH1cclxuXHJcbn1cclxuIiwiaW1wb3J0IEFkZENhcmRHZW5lcmF0b3IgZnJvbSAnLi9BZGRDYXJkR2VuZXJhdG9yJztcclxuaW1wb3J0IEFkZENhdGVnb3J5R2VuZXJhdG9yIGZyb20gJy4vQWRkQ2F0ZWdvcnlHZW5lcmF0b3InO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQWRkU2VydmVyRGF0YSB7XHJcblx0Y29uc3RydWN0b3IoaW5pdCkge1xyXG5cdFx0dGhpcy5zZWxlY3RvciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoaW5pdC5zZWxlY3Rvcik7XHJcblx0XHR0aGlzLmRhdGFVcmwgPSBpbml0LmRhdGFVcmw7XHJcblx0XHR0aGlzLmxvYWRlciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5sb2FkZXJfX2JveCcpO1xyXG5cdFx0dGhpcy5tYWluU2VsZWN0b3IgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdNQUlOJyk7XHJcblx0XHR0aGlzLmlucHV0U2VhcmNoID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignaW5wdXRbbmFtZT1zZWFyY2hdJyk7XHJcblx0XHR0aGlzLmNsaWNrSW5wdXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuanNfX2NsaWNrRW50ZXInKTtcclxuXHRcdHRoaXMuc2VsZWN0b3JOb3RoaW5nID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmpzX19yZXN1bHROb3RoaW5nJyk7XHJcblx0XHRcclxuXHRcdHRoaXMuY2FyZFRvdWNoID0gZmFsc2U7XHJcblx0XHR0aGlzLmNsaWNrZWRUYXJnZXQgPSAnJztcclxuXHRcdHRoaXMuY2xpY2tlZFRhcmdldE5hbWUgPSAnJztcclxuXHRcdFxyXG5cdFx0dGhpcy5yZW1vdmVMb29wQ2xpY2sgPSAnJztcclxuXHRcdFxyXG5cdFx0dGhpcy5oaWRkZW5VcmwgPSAnJztcclxuXHJcblx0XHR0aGlzLmZpbHRlckZsYWcgPSBCb29sZWFuKGZhbHNlKTtcclxuXHRcdHRoaXMuZXJyb3JUZXh0ID0gJ/CfkoAgT3VjaCwgZGF0YWJhc2UgZXJyb3IuLi4nO1xyXG5cdH1cclxuXHJcblx0c3RhdGljIGluZm8oKSB7IGNvbnNvbGUubG9nKCfigKIgTU9EVUxFOicsIHRoaXMubmFtZSwgdHJ1ZSk7IH1cclxuXHJcblx0cnVuKCkge1xyXG5cdFx0aWYgKHRoaXMuc2VsZWN0b3IpIHtcclxuXHRcdFx0dGhpcy5jb25zdHJ1Y3Rvci5pbmZvKCk7XHJcblx0XHRcdHRoaXMuZmV0Y2hNeURhdGEoKTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdHN1Y2Nlc3NMb2FkaW5nKCkge1xyXG5cdFx0Y29uc3QgbGlzdGluZXIgPSAoKSA9PiB0aGlzLmxvYWRlci5jbGFzc0xpc3QudG9nZ2xlKCdoaWRkZW4nKTtcclxuXHRcdHNldFRpbWVvdXQobGlzdGluZXIsIDEwMCk7XHJcblx0fVxyXG5cclxuXHRldmVudEhhbmRsZXJzKCkge1xyXG5cdFx0dGhpcy5tYWluU2VsZWN0b3IuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBldmVudCA9PiB0aGlzLmNsb3NlQW5kT3BlbkZpbHRlcihldmVudCksIGZhbHNlKTtcclxuXHRcdHRoaXMuaW5wdXRTZWFyY2guYWRkRXZlbnRMaXN0ZW5lcignaW5wdXQnLCBldmVudCA9PiB0aGlzLmxpdmVTZWFyY2goZXZlbnQpLCBmYWxzZSk7XHJcblx0XHR0aGlzLmlucHV0U2VhcmNoLmFkZEV2ZW50TGlzdGVuZXIoJ2tleWRvd24nLCBldmVudCA9PiB0aGlzLmtleWJvYXJkRW50ZXIoZXZlbnQpLCBmYWxzZSk7XHJcblx0XHR0aGlzLmNsaWNrSW5wdXQuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBldmVudCA9PiB0aGlzLmtleWJvYXJkRW50ZXJDbGljayhldmVudCksIGZhbHNlKTtcclxuXHR9XHJcblx0XHJcblx0X19jbGVhblNlYXJjaFJlc3VsdCgpIHtcclxuXHRcdHRoaXMubGluayA9IFsuLi5kb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuc3RhcnRwYWdlX19zdHVmZi1pdGVtLmxvYWQnKV07XHJcblx0XHR0aGlzLnJlbmRlciA9IHRoaXMubGluay5maWx0ZXIoZiA9PiBmLmNsYXNzTGlzdC5yZW1vdmUoJ2xvYWQnKSk7XHJcblx0fVxyXG5cdFxyXG5cdF9yZXN1bHRFbnRlcigpIHtcclxuXHRcdGlmICh0aGlzLnJlbW92ZUxvb3BDbGljayA9PT0gdGhpcy5lbnRlclRleHQpIHJldHVybjtcclxuXHRcdFxyXG5cdFx0dGhpcy5fX2NsZWFuU2VhcmNoUmVzdWx0KCk7XHJcblx0XHR0aGlzLmxpbmsgPSBbLi4uZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLnN0dWZmY2FyZF9fYnV0dG9uJyldO1xyXG5cdFx0dGhpcy5yZXN1bHQgPSB0aGlzLmxpdmVTZWFyY2hSZXN1bHQgfHwgWycnXTtcclxuXHRcdHRoaXMudG90YWxSZXN1bHQgPSB0aGlzLmxpbmsuZmlsdGVyKGYgPT4gdGhpcy5yZXN1bHQuc29tZShzID0+IE51bWJlcihmLmhyZWYuc3BsaXQoJyMnKVsxXSkgPT09IHMpKTtcclxuXHRcdHRoaXMudG90YWxSZXN1bHQubWFwKGYgPT4gZi5jbG9zZXN0KCcuc3RhcnRwYWdlX19zdHVmZi1pdGVtJykuY2xhc3NMaXN0LmFkZCgnbG9hZCcpKTtcclxuXHRcdFxyXG5cdFx0dGhpcy5yZW1vdmVMb29wQ2xpY2sgPSB0aGlzLmVudGVyVGV4dDtcclxuXHRcdHRoaXMuX3JlbmRlclplcm9SZXN1bHQoKTtcclxuXHR9XHJcblx0XHJcblx0a2V5Ym9hcmRFbnRlckNsaWNrKGV2ZW50KSB7XHJcblx0XHR0aGlzLl9yZXN1bHRFbnRlcigpO1xyXG5cdH1cclxuXHRcclxuXHRrZXlib2FyZEVudGVyKGV2ZW50KSB7XHJcblx0XHR0aGlzLmVudGVyQ29kZSA9ICdFbnRlcic7XHJcblx0XHRpZiAoZXZlbnQua2V5ID09PSB0aGlzLmVudGVyQ29kZSkgdGhpcy5fcmVzdWx0RW50ZXIoKTtcclxuXHR9XHJcblxyXG5cdGxpdmVTZWFyY2goKSB7XHJcblx0XHR0aGlzLmVudGVyVGV4dCA9IFN0cmluZyh0aGlzLmlucHV0U2VhcmNoLnZhbHVlKS50cmltKCk7XHJcblx0XHR0aGlzLnJlZ01hZ2ljID0gL1xccy9nO1xyXG5cdFx0dGhpcy5zcGxpdEVudGVyVGV4dCA9IHRoaXMuZW50ZXJUZXh0LnRvTG93ZXJDYXNlKCkuc3BsaXQodGhpcy5yZWdNYWdpYykubWFwKFN0cmluZyk7XHJcblx0XHR0aGlzLnRleHRGaWx0ZXIgPSBmID0+IHRoaXMuc3BsaXRFbnRlclRleHQuZXZlcnkocyA9PiBmLnRpdGxlLmluY2x1ZGVzKHMpKTtcclxuXHRcdHRoaXMubGl2ZVNlYXJjaFJlc3VsdCA9IHRoaXMubWFpbkRhdGFMb3dlci5maWx0ZXIodGhpcy50ZXh0RmlsdGVyKS5tYXAoZiA9PiBOdW1iZXIoZi5jb3Vyc2VJZCkpO1xyXG5cdH1cclxuXHJcblx0Y2xvc2VBbmRPcGVuRmlsdGVyKGV2ZW50KSB7XHJcblx0XHR0aGlzLl90YXJnZXQgPSBldmVudC50YXJnZXQ7XHJcblx0XHRpZiAodGhpcy5fdGFyZ2V0LmlkICE9PSB0aGlzLnJlbW92ZUxvb3BDbGljaykge1xyXG5cdFx0XHR0aGlzLm1haW5TZWxlY3Rvci5xdWVyeVNlbGVjdG9yQWxsKCcuanNfX2NyZWF0ZURyb3BMaXN0IFVMLm9wZW4nKS5mb3JFYWNoKGYgPT4gZi5jbGFzc0xpc3QucmVtb3ZlKCdvcGVuJykpO1xyXG5cdFx0fVxyXG5cclxuXHRcdGlmICh0aGlzLl90YXJnZXQubWF0Y2hlcygnLmRyb3Bkb3duX19sYWJlbCcpKSB7XHJcblx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcblx0XHRcdGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xyXG5cdFx0fVxyXG5cclxuXHRcdGlmICh0aGlzLl90YXJnZXQubWF0Y2hlcygnSU5QVVRbdHlwZT1idXR0b25dJykpIHtcclxuXHRcdFx0dGhpcy5fdGFyZ2V0LnBhcmVudEVsZW1lbnQucXVlcnlTZWxlY3RvcignVUwnKS5jbGFzc0xpc3QuYWRkKCdvcGVuJyk7XHJcblx0XHRcdHRoaXMucmVtb3ZlTG9vcENsaWNrID0gdGhpcy5fdGFyZ2V0LmlkO1xyXG5cdFx0XHR0aGlzLmNsaWNrZWRUYXJnZXROYW1lID0gdGhpcy5fdGFyZ2V0LnBhcmVudEVsZW1lbnQucXVlcnlTZWxlY3RvcignSU5QVVQnKS5uYW1lO1xyXG5cclxuXHRcdFx0dGhpcy5oaWRkZW5VcmwgPSB0aGlzLl90YXJnZXQudmFsdWU7XHJcblx0XHR9XHJcblxyXG5cdFx0aWYgKHRoaXMuX3RhcmdldC5ub2RlTmFtZSA9PT0gJ0xJJykge1xyXG5cdFx0XHR0aGlzLmdyYWJUZXh0ID0gdGhpcy5fdGFyZ2V0LnRleHRDb250ZW50O1xyXG5cdFx0XHR0aGlzLmNsaWNrZWRUYXJnZXQgPSB0aGlzLmdyYWJUZXh0O1xyXG5cdFx0XHRpZiAodGhpcy5oaWRkZW5VcmwgPT09IHRoaXMuZ3JhYlRleHQpIHJldHVybjtcclxuXHJcblx0XHRcdHRoaXMucGFzdGVJblNlbGVjdG9yID0gdGhpcy5fdGFyZ2V0LnBhcmVudEVsZW1lbnQucGFyZW50RWxlbWVudDtcclxuXHRcdFx0dGhpcy5wYXN0ZUluU2VsZWN0b3IucXVlcnlTZWxlY3RvcignSU5QVVRbdHlwZT1idXR0b25dJykudmFsdWUgPSB0aGlzLmdyYWJUZXh0IHx8ICcnO1xyXG5cdFx0XHR0aGlzLl9jbG9zZUFuZE9wZW5TYXZlUmVzdWx0KCk7XHJcblx0XHRcdHRoaXMuX3JlbmRlclJlc3VsdEFsdCgpO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0X2Nsb3NlQW5kT3BlblNhdmVSZXN1bHQoKSB7XHJcblx0XHR0aGlzLmRlZmF1bHRJbnB1dFZhbHVlID0gWy4uLmRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJ0lOUFVUW3R5cGU9YnV0dG9uXScpXS5tYXAoZiA9PiBmLnZhbHVlKTtcclxuXHR9XHJcblxyXG5cdF9fcmVuZGVyUmVzdWx0R3JhZGUoKSB7XHJcblx0XHR0aGlzLnJlc3VsdCA9IHRoaXMubWFpbkRhdGEuZmlsdGVyKGYgPT4gZi5ncmFkZS5zcGxpdCgnOycpLmluY2x1ZGVzKHRoaXMuX3RhcmdldCkpLm1hcChmID0+IE51bWJlcihmLmNvdXJzZUlkKSk7XHJcblx0XHR0aGlzLmxpbmsgPSBbLi4uZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLnN0dWZmY2FyZF9fYnV0dG9uJyldO1xyXG5cdFx0dGhpcy5ncmFkZVJlc3VsdCA9IHRoaXMubGluay5maWx0ZXIoZiA9PiB0aGlzLnJlc3VsdC5zb21lKHMgPT4gTnVtYmVyKGYuaHJlZi5zcGxpdCgnIycpWzFdKSA9PT0gcykpO1xyXG5cdFx0dGhpcy5ncmFkZVJlc3VsdC5tYXAoZiA9PiBmLmNsb3Nlc3QoJy5zdGFydHBhZ2VfX3N0dWZmLWl0ZW0nKS5jbGFzc0xpc3QuYWRkKCdsb2FkJykpO1xyXG5cdH1cclxuXHJcblx0X19yZW5kZXJSZXN1bHRTdWJqZWN0KCkge1xyXG5cdFx0dGhpcy5saW5rID0gWy4uLmRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoYC5zdHVmZmNhcmRfXyR7dGhpcy5jbGlja2VkVGFyZ2V0TmFtZX1gKV07XHJcblx0XHR0aGlzLnN1YmplY3RSZXN1bHQgPSB0aGlzLmxpbmsuZmlsdGVyKGYgPT4gZi50ZXh0Q29udGVudC5pbmNsdWRlcyh0aGlzLl90YXJnZXQpKTtcclxuXHRcdHRoaXMuc3ViamVjdFJlc3VsdC5tYXAoZiA9PiBmLmNsb3Nlc3QoJy5zdGFydHBhZ2VfX3N0dWZmLWl0ZW0nKS5jbGFzc0xpc3QuYWRkKCdsb2FkJykpO1xyXG5cdH1cclxuXHJcblx0X3JlbmRlclJlc3VsdEFsdCgpIHtcclxuXHRcdHRoaXMuX19jbGVhblNlYXJjaFJlc3VsdCgpO1xyXG5cdFx0dGhpcy5wYXNzZWRSZXN1bHQgPSB0aGlzLmRlZmF1bHRJbnB1dFZhbHVlLm1hcCgoZiwgaW5kZXgpID0+IChcclxuXHRcdFx0KHRoaXMuZGVmYXVsdElucHV0VmFsdWVbaW5kZXhdID09PSB0aGlzLmxpbmtEZWZhdWx0VmFsdWVbaW5kZXhdKSA/ICcnIDogdGhpcy5kZWZhdWx0SW5wdXRWYWx1ZVtpbmRleF0pKTtcclxuXHRcdFxyXG5cdFx0dGhpcy5jaGFuZ2VSZXN1bHRBZnRlckNsaWNrID0gKHRhcmdldCwgbnVtYmVyKSA9PiB7XHJcblx0XHRcdGlmICh0aGlzLmRlZmF1bHRJbnB1dFZhbHVlW251bWJlcl0gPT09IHRoaXMubGlua0RlZmF1bHRWYWx1ZVtudW1iZXJdKSByZXR1cm4gdGhpcy5kZWZhdWx0SW5wdXRWYWx1ZVtudW1iZXJdO1xyXG5cdFx0XHRpZiAobnVtYmVyID09PSAyKSByZXR1cm4gdGFyZ2V0W3RoaXMuZGVmYXVsdElucHV0TmFtZVtudW1iZXJdXS5zcGxpdCgnOycpLmluY2x1ZGVzKHRoaXMucGFzc2VkUmVzdWx0W251bWJlcl0pO1xyXG5cdFx0XHRyZXR1cm4gdGFyZ2V0W3RoaXMuZGVmYXVsdElucHV0TmFtZVtudW1iZXJdXS5pbmNsdWRlcyh0aGlzLnBhc3NlZFJlc3VsdFtudW1iZXJdKTtcclxuXHRcdH07XHJcblxyXG5cdFx0dGhpcy5yZXN1bHQgPSB0aGlzLm1haW5EYXRhXHJcblx0XHRcdC5maWx0ZXIoZiA9PiB0aGlzLmNoYW5nZVJlc3VsdEFmdGVyQ2xpY2soZiwgMCkpXHJcblx0XHRcdC5maWx0ZXIoZiA9PiB0aGlzLmNoYW5nZVJlc3VsdEFmdGVyQ2xpY2soZiwgMSkpXHJcblx0XHRcdC5maWx0ZXIoZiA9PiB0aGlzLmNoYW5nZVJlc3VsdEFmdGVyQ2xpY2soZiwgMikpXHJcblx0XHRcdC5tYXAoZiA9PiBOdW1iZXIoZi5jb3Vyc2VJZCkpO1xyXG5cdFx0dGhpcy5saW5rID0gWy4uLmRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5zdHVmZmNhcmRfX2J1dHRvbicpXTtcclxuXHRcdHRoaXMudG90YWxSZXN1bHQgPSB0aGlzLmxpbmsuZmlsdGVyKGYgPT4gdGhpcy5yZXN1bHQuc29tZShzID0+IE51bWJlcihmLmhyZWYuc3BsaXQoJyMnKVsxXSkgPT09IHMpKTtcclxuXHRcdHRoaXMudG90YWxSZXN1bHQubWFwKGYgPT4gZi5jbG9zZXN0KCcuc3RhcnRwYWdlX19zdHVmZi1pdGVtJykuY2xhc3NMaXN0LmFkZCgnbG9hZCcpKTtcclxuXHRcdFxyXG5cdFx0dGhpcy5fcmVuZGVyWmVyb1Jlc3VsdCgpO1xyXG5cdH1cclxuXHJcblx0X3JlbmRlclplcm9SZXN1bHQoKSB7XHJcblx0XHRpZiAodGhpcy5yZXN1bHQubGVuZ3RoIDw9IDApIHRoaXMuc2VsZWN0b3JOb3RoaW5nLmNsYXNzTGlzdC5hZGQoJ3Nob3cnKTtcclxuXHRcdGVsc2UgdGhpcy5zZWxlY3Rvck5vdGhpbmcuY2xhc3NMaXN0LnJlbW92ZSgnc2hvdycpO1xyXG5cdH1cclxuXHRcclxuXHRyZW5kZXJDYXJkcygpIHtcclxuXHRcdHRoaXMubGluayA9IFsuLi5kb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuc3RhcnRwYWdlX19zdHVmZi1pdGVtJyldO1xyXG5cdFx0dGhpcy5yZW5kZXIgPSB0aGlzLmxpbmsuZmlsdGVyKGYgPT4gZi5jbGFzc0xpc3QuYWRkKCdsb2FkJykpO1xyXG5cdH1cclxuXHRcclxuXHRhZnRlckJ1aWxkaW5nQ2FyZHMoKSB7XHJcblx0XHR0aGlzLnJlbmRlckNhcmRzKCk7XHJcblx0XHR0aGlzLmxpbmtEZWZhdWx0VmFsdWUgPSBbLi4uZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnW3R5cGU9YnV0dG9uXScpXS5tYXAoZiA9PiBmLnZhbHVlKTtcclxuXHRcdHRoaXMuZGVmYXVsdElucHV0TmFtZSA9IFsuLi5kb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCdJTlBVVFt0eXBlPWJ1dHRvbl0nKV0ubWFwKGYgPT4gZi5uYW1lKTtcclxuXHRcdHRoaXMubGl2ZVNlYXJjaCgpO1xyXG5cdH1cclxuXHJcblx0YnVpbGRGaWx0ZXJMaXN0KCkge1xyXG5cdFx0Y29uc3Qgc2VsZWN0QWxsRmlsdGVycyA9IFsuLi5kb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCdpbnB1dFt0eXBlPWJ1dHRvbl0nKV07XHJcblx0XHR0aGlzLmNhdGVnb3J5Q2xlYW5EYXRhID0gc2VsZWN0QWxsRmlsdGVycy5tYXAoKGYsIGluZGV4KSA9PiAoe1xyXG5cdFx0XHRzZWxlY3RvcklEOiBzZWxlY3RBbGxGaWx0ZXJzW2luZGV4XS5pZCxcclxuXHRcdFx0c2VsZWN0b3JOYW1lOiBzZWxlY3RBbGxGaWx0ZXJzW2luZGV4XS5uYW1lLFxyXG5cdFx0XHRzZWxlY3RvclZhbHVlOiBzZWxlY3RBbGxGaWx0ZXJzW2luZGV4XS52YWx1ZVxyXG5cdFx0fSkpO1xyXG5cclxuXHRcdGNvbnN0IG5ld0NhdGVnb3J5ID0geCA9PiBuZXcgQWRkQ2F0ZWdvcnlHZW5lcmF0b3IoeCk7XHJcblx0XHR0aGlzLmNhdGVnb3J5Q2xlYW5EYXRhLm1hcCgoZiwgaW5kZXgpID0+IG5ld0NhdGVnb3J5KHtcclxuXHRcdFx0c2VsZWN0b3I6IGAjJHtmLnNlbGVjdG9ySUR9YCxcclxuXHRcdFx0YXJyYXk6IHRoaXMubWFpbkRhdGEsXHJcblx0XHRcdGNhdGVnb3J5OiBmLnNlbGVjdG9yTmFtZSxcclxuXHRcdFx0dG9vbHRpcDogZi5zZWxlY3RvclZhbHVlXHJcblx0XHR9KSk7XHJcblx0fVxyXG5cclxuXHRmZXRjaE15RGF0YSgpIHtcclxuXHRcdGNvbnN0IHNlbmRSZXF1ZXN0ID0gSlNPTi5zdHJpbmdpZnkoeyBkYXRhOiAnJyB9LCBudWxsLCAnXFx0Jyk7XHJcblx0XHRjb25zdCBvcHRpb25zID0ge1xyXG5cdFx0XHRtZXRob2Q6ICdQT1NUJyxcclxuXHRcdFx0aGVhZGVyczogeyAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nIH0sXHJcblx0XHRcdG1vZGU6ICdjb3JzJyxcclxuXHRcdFx0Y2FjaGU6ICdkZWZhdWx0JyxcclxuXHRcdFx0Ym9keTogc2VuZFJlcXVlc3RcclxuXHRcdH07XHJcblxyXG5cdFx0Y29uc3Qgb3JpZ2luVXJsID0gdGhpcy5kYXRhVXJsO1xyXG5cdFx0Y29uc3Qgbm9DT1JTID0gJ2h0dHBzOi8vY29ycy1hbnl3aGVyZS5oZXJva3VhcHAuY29tLyc7XHJcblx0XHRjb25zdCB1cmwgPSBgJHtub0NPUlN9JHtvcmlnaW5Vcmx9YDtcclxuXHRcdC8vIGNvbnN0IHVybCA9IGAke29yaWdpblVybH1gO1xyXG5cclxuXHRcdGZldGNoKHVybCwgb3B0aW9ucylcclxuXHRcdFx0LnRoZW4ocmVzcG9uc2UgPT4gcmVzcG9uc2UuanNvbigpKVxyXG5cdFx0XHQudGhlbigoZGF0YSkgPT4ge1xyXG5cdFx0XHRcdHRoaXMubWFpbkRhdGEgPSBbLi4uZGF0YS5pdGVtc107XHJcblx0XHRcdFx0dGhpcy5tYWluRGF0YUxvd2VyID0gdGhpcy5tYWluRGF0YS5tYXAoZiA9PiAoeyAuLi5mLCB0aXRsZTogZi50aXRsZS50b0xvd2VyQ2FzZSgpIH0pKTtcclxuXHJcblx0XHRcdFx0Y29uc3QgbmV3Q2FyZCA9IHggPT4gbmV3IEFkZENhcmRHZW5lcmF0b3IoeCk7XHJcblx0XHRcdFx0dGhpcy5tYWluRGF0YS5tYXAoZiA9PiBuZXdDYXJkKHtcclxuXHRcdFx0XHRcdHNlbGVjdG9yOiB0aGlzLnNlbGVjdG9yLFxyXG5cdFx0XHRcdFx0cGljdHVyZTogZi5jb3Vyc2VJZCxcclxuXHRcdFx0XHRcdHRpdGxlOiBmLnN1YmplY3QsXHJcblx0XHRcdFx0XHRncmFkZTogZi5ncmFkZSxcclxuXHRcdFx0XHRcdGdlbnJlOiBmLmdlbnJlLFxyXG5cdFx0XHRcdFx0ZXh0cmE6IGAjJHtmLnNob3BVcmx9YCxcclxuXHRcdFx0XHRcdGJ1dHRvbjogYCMke2YuY291cnNlSWR9YFxyXG5cdFx0XHRcdH0pKTtcclxuXHRcdFx0fSlcclxuXHRcdFx0LnRoZW4odGhpcy5idWlsZEZpbHRlckxpc3QuYmluZCh0aGlzKSlcclxuXHRcdFx0LnRoZW4odGhpcy5hZnRlckJ1aWxkaW5nQ2FyZHMuYmluZCh0aGlzKSlcclxuXHRcdFx0LnRoZW4odGhpcy5ldmVudEhhbmRsZXJzLmJpbmQodGhpcykpXHJcblx0XHRcdC50aGVuKHRoaXMuc3VjY2Vzc0xvYWRpbmcuYmluZCh0aGlzKSk7XHJcblx0XHQvLyAuY2F0Y2gob25SZWplY3RlZCA9PiBjb25zb2xlLmVycm9yKHRoaXMuZXJyb3JUZXh0LCBgXFxuJHtvblJlamVjdGVkLm1lc3NhZ2V9YCkpO1xyXG5cdH1cclxuXHJcbn1cclxuIiwiZXhwb3J0IGRlZmF1bHQgY2xhc3MgQWRkTW92ZVNjcm9sbFVQIHtcclxuXHRjb25zdHJ1Y3Rvcihpbml0KSB7XHJcblx0XHR0aGlzLnNlbGVjdG9yID0gZG9jdW1lbnQucXVlcnlTZWxlY3Rvcihpbml0LnNlbGVjdG9yKTtcclxuXHRcdHRoaXMuY291bnQgPSBOdW1iZXIoTWF0aC5hYnMoaW5pdC5zcGVlZCkpIHx8IDg7XHJcblx0XHR0aGlzLnNwZWVkID0gKHRoaXMuY291bnQgPD0gMjApID8gdGhpcy5jb3VudCA6IDg7XHJcblx0XHR0aGlzLm15UG9zID0gd2luZG93LnBhZ2VZT2Zmc2V0O1xyXG5cdFx0dGhpcy5nZXRTY3JvbGwgPSAwOyBcclxuXHRcdHRoaXMuc3BlZWRTY3JvbGwgPSB0aGlzLnNwZWVkO1xyXG5cdH1cclxuXHJcblx0c3RhdGljIGluZm8oKSB7IGNvbnNvbGUubG9nKCfigKIgTU9EVUxFOicsIHRoaXMubmFtZSwgdHJ1ZSk7IH1cclxuXHJcblx0c2Nyb2xsVXBTZXR0aW5nKCkge1xyXG5cdFx0Y29uc3QgY2xpY2tlZEFycm93ID0gKCkgPT4ge1xyXG5cdFx0XHR0aGlzLmdldFNjcm9sbCA9IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zY3JvbGxUb3A7IFxyXG5cdFx0XHRpZiAodGhpcy5nZXRTY3JvbGwgPj0gMSkge1xyXG5cdFx0XHRcdHdpbmRvdy5yZXF1ZXN0QW5pbWF0aW9uRnJhbWUoY2xpY2tlZEFycm93KTtcclxuXHRcdFx0XHR3aW5kb3cuc2Nyb2xsVG8oMCwgdGhpcy5nZXRTY3JvbGwgLSB0aGlzLmdldFNjcm9sbCAvIHRoaXMuc3BlZWRTY3JvbGwpO1xyXG5cdFx0XHR9XHJcblx0XHR9O1xyXG5cdFx0Y29uc3Qgc2Nyb2xsZWREb3duID0gKCkgPT4ge1xyXG5cdFx0XHR0aGlzLm15UG9zID0gd2luZG93LnBhZ2VZT2Zmc2V0O1xyXG5cdFx0XHQodGhpcy5teVBvcyA+PSAxMDApID8gdGhpcy5zZWxlY3Rvci5jbGFzc0xpc3QuYWRkKCdzaG93JykgOiB0aGlzLnNlbGVjdG9yLmNsYXNzTGlzdC5yZW1vdmUoJ3Nob3cnKTtcclxuXHRcdH07XHJcblxyXG5cdFx0dGhpcy5zZWxlY3Rvci5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGNsaWNrZWRBcnJvdywgZmFsc2UpO1xyXG5cdFx0d2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIHNjcm9sbGVkRG93biwgZmFsc2UpO1xyXG5cdH1cclxuXHJcblx0cnVuKCkge1xyXG5cdFx0aWYgKHRoaXMuc2VsZWN0b3IpIHtcclxuXHRcdFx0dGhpcy5jb25zdHJ1Y3Rvci5pbmZvKCk7XHJcblx0XHRcdHRoaXMuc2Nyb2xsVXBTZXR0aW5nKCk7XHJcblx0XHR9XHJcblx0fVxyXG59XHJcbiIsImV4cG9ydCBkZWZhdWx0IGNsYXNzIEFkZE9wZW5NeUJ1cmdlciB7XHJcblx0Y29uc3RydWN0b3IoaW5pdCkge1xyXG5cdFx0dGhpcy5zZWxlY3RvciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoaW5pdC5idXJnZXIpO1xyXG5cdFx0dGhpcy5uYXZiYXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGluaXQubmF2YmFyKTtcclxuXHRcdHRoaXMuY2xvbmVNZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoaW5pdC5saXN0KTtcclxuXHRcdHRoaXMuZXJyb3JUZXh0ID0gJ/CfkoAgT3VjaCwgZGF0YWJhc2UgZXJyb3IuLi4nO1xyXG5cdH1cclxuXHJcblx0c3RhdGljIGluZm8oKSB7IGNvbnNvbGUubG9nKCfigKIgTU9EVUxFOicsIHRoaXMubmFtZSwgdHJ1ZSk7IH1cclxuXHRcclxuXHRydW4oKSB7XHJcblx0XHRpZiAodGhpcy5zZWxlY3Rvcikge1xyXG5cdFx0XHR0aGlzLmNvbnN0cnVjdG9yLmluZm8oKTtcclxuXHRcdFx0dGhpcy5jcmVhdGVDb3B5TGlua3MoKTtcclxuXHRcdFx0dGhpcy5ldmVudEhhbmRsZXJzKCk7XHJcblx0XHR9XHJcblx0fVxyXG5cdFxyXG5cdGV2ZW50SGFuZGxlcnMoKSB7XHJcblx0XHR0aGlzLnNlbGVjdG9yLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZXZlbnQgPT4gdGhpcy50b2dnbGVCdXJnZXIoZXZlbnQpLCBmYWxzZSk7XHJcblx0fVxyXG5cdFxyXG5cdGNyZWF0ZUNvcHlMaW5rcygpIHtcclxuXHRcdHRyeSB7XHJcblx0XHRcdHRoaXMubmV3Q2xvbmVEaXYgPSBkb2N1bWVudC5pbXBvcnROb2RlKHRoaXMubmF2YmFyLCB0cnVlKTtcclxuXHRcdFx0dGhpcy5uZXdDbG9uZURpdi5jbGFzc0xpc3QuYWRkKCdteWhlYWRlcl9fbGlzdC0tY2xvbmVkJyk7XHJcblx0XHRcdHRoaXMubmV3Q2xvbmVEaXYuY2xhc3NMaXN0LnJlbW92ZSgnbXloZWFkZXJfX2xpc3QnKTtcclxuXHRcdFx0dGhpcy5jbG9uZU1lLmFmdGVyKHRoaXMubmV3Q2xvbmVEaXYpO1xyXG5cdFx0fSBjYXRjaCAoZXJyb3IpIHtcclxuXHRcdFx0Y29uc29sZS5lcnJvcignW0VSUk9SXSBIYW1idXJnZXIgbGlzdCBpcyBlbXB0eSEnKTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdHRvZ2dsZUJ1cmdlcigpIHtcclxuXHRcdHRoaXMuc2VsZWN0b3IuY2xhc3NMaXN0LnRvZ2dsZSgnb3BlbicpO1xyXG5cdFx0aWYgKHdpbmRvdy5pbm5lcldpZHRoIDw9IDY3OCkgdGhpcy5uYXZiYXIuY2xhc3NMaXN0LnRvZ2dsZSgnb3BlbicpO1xyXG5cdFx0aWYgKHdpbmRvdy5pbm5lcldpZHRoID49IDY3OCkgdGhpcy5uZXdDbG9uZURpdi5jbGFzc0xpc3QudG9nZ2xlKCdvcGVuJyk7XHJcblx0fVxyXG59XHJcbiJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUNsRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMvQkE7OztBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7OztBQUdBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFlQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQXhDQTtBQUFBO0FBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNoQkE7OztBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTs7O0FBbkNBO0FBQUE7QUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2ZBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQUE7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBRUE7OztBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFBQTtBQUNBO0FBS0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBTUE7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVBBO0FBQUE7QUFTQTtBQU1BOzs7QUF0TUE7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3pCQTs7O0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUF4QkE7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1ZBOzs7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQTdCQTtBQUFBO0FBQUE7Ozs7Ozs7Ozs7QSIsInNvdXJjZVJvb3QiOiIifQ==