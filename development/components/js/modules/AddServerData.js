import AddCardGenerator from './AddCardGenerator';
import AddCategoryGenerator from './AddCategoryGenerator';

export default class AddServerData {
	constructor(init) {
		this.selector = document.querySelector(init.selector);
		this.dataUrl = init.dataUrl;
		this.loader = document.querySelector('.loader__box');
		this.mainSelector = document.querySelector('MAIN');
		this.inputSearch = document.querySelector('input[name=search]');
		this.clickInput = document.querySelector('.js__clickEnter');
		this.selectorNothing = document.querySelector('.js__resultNothing');
		
		this.cardTouch = false;
		this.clickedTarget = '';
		this.clickedTargetName = '';
		
		this.removeLoopClick = '';
		
		this.hiddenUrl = '';

		this.filterFlag = Boolean(false);
		this.errorText = '💀 Ouch, database error...';
	}

	static info() { console.log('• MODULE:', this.name, true); }

	run() {
		if (this.selector) {
			this.constructor.info();
			this.fetchMyData();
		}
	}

	successLoading() {
		const listiner = () => this.loader.classList.toggle('hidden');
		setTimeout(listiner, 100);
	}

	eventHandlers() {
		this.mainSelector.addEventListener('click', event => this.closeAndOpenFilter(event), false);
		this.inputSearch.addEventListener('input', event => this.liveSearch(event), false);
		this.inputSearch.addEventListener('keydown', event => this.keyboardEnter(event), false);
		this.clickInput.addEventListener('click', event => this.keyboardEnterClick(event), false);
	}
	
	__cleanSearchResult() {
		this.link = [...document.querySelectorAll('.startpage__stuff-item.load')];
		this.render = this.link.filter(f => f.classList.remove('load'));
	}
	
	_resultEnter() {
		if (this.removeLoopClick === this.enterText) return;
		
		this.__cleanSearchResult();
		this.link = [...document.querySelectorAll('.stuffcard__button')];
		this.result = this.liveSearchResult || [''];
		this.totalResult = this.link.filter(f => this.result.some(s => Number(f.href.split('#')[1]) === s));
		this.totalResult.map(f => f.closest('.startpage__stuff-item').classList.add('load'));
		
		this.removeLoopClick = this.enterText;
		this._renderZeroResult();
	}
	
	keyboardEnterClick(event) {
		this._resultEnter();
	}
	
	keyboardEnter(event) {
		this.enterCode = 'Enter';
		if (event.key === this.enterCode) this._resultEnter();
	}

	liveSearch() {
		this.enterText = String(this.inputSearch.value).trim();
		this.regMagic = /\s/g;
		this.splitEnterText = this.enterText.toLowerCase().split(this.regMagic).map(String);
		this.textFilter = f => this.splitEnterText.every(s => f.title.includes(s));
		this.liveSearchResult = this.mainDataLower.filter(this.textFilter).map(f => Number(f.courseId));
	}

	closeAndOpenFilter(event) {
		this._target = event.target;
		if (this._target.id !== this.removeLoopClick) {
			this.mainSelector.querySelectorAll('.js__createDropList UL.open').forEach(f => f.classList.remove('open'));
		}

		if (this._target.matches('.dropdown__label')) {
			event.preventDefault();
			event.stopPropagation();
		}

		if (this._target.matches('INPUT[type=button]')) {
			this._target.parentElement.querySelector('UL').classList.add('open');
			this.removeLoopClick = this._target.id;
			this.clickedTargetName = this._target.parentElement.querySelector('INPUT').name;

			this.hiddenUrl = this._target.value;
		}

		if (this._target.nodeName === 'LI') {
			this.grabText = this._target.textContent;
			this.clickedTarget = this.grabText;
			if (this.hiddenUrl === this.grabText) return;

			this.pasteInSelector = this._target.parentElement.parentElement;
			this.pasteInSelector.querySelector('INPUT[type=button]').value = this.grabText || '';
			this._closeAndOpenSaveResult();
			this._renderResultAlt();
		}
	}

	_closeAndOpenSaveResult() {
		this.defaultInputValue = [...document.querySelectorAll('INPUT[type=button]')].map(f => f.value);
	}

	__renderResultGrade() {
		this.result = this.mainData.filter(f => f.grade.split(';').includes(this._target)).map(f => Number(f.courseId));
		this.link = [...document.querySelectorAll('.stuffcard__button')];
		this.gradeResult = this.link.filter(f => this.result.some(s => Number(f.href.split('#')[1]) === s));
		this.gradeResult.map(f => f.closest('.startpage__stuff-item').classList.add('load'));
	}

	__renderResultSubject() {
		this.link = [...document.querySelectorAll(`.stuffcard__${this.clickedTargetName}`)];
		this.subjectResult = this.link.filter(f => f.textContent.includes(this._target));
		this.subjectResult.map(f => f.closest('.startpage__stuff-item').classList.add('load'));
	}

	_renderResultAlt() {
		this.__cleanSearchResult();
		this.passedResult = this.defaultInputValue.map((f, index) => (
			(this.defaultInputValue[index] === this.linkDefaultValue[index]) ? '' : this.defaultInputValue[index]));
		
		this.changeResultAfterClick = (target, number) => {
			if (this.defaultInputValue[number] === this.linkDefaultValue[number]) return this.defaultInputValue[number];
			if (number === 2) return target[this.defaultInputName[number]].split(';').includes(this.passedResult[number]);
			return target[this.defaultInputName[number]].includes(this.passedResult[number]);
		};

		this.result = this.mainData
			.filter(f => this.changeResultAfterClick(f, 0))
			.filter(f => this.changeResultAfterClick(f, 1))
			.filter(f => this.changeResultAfterClick(f, 2))
			.map(f => Number(f.courseId));
		this.link = [...document.querySelectorAll('.stuffcard__button')];
		this.totalResult = this.link.filter(f => this.result.some(s => Number(f.href.split('#')[1]) === s));
		this.totalResult.map(f => f.closest('.startpage__stuff-item').classList.add('load'));
		
		this._renderZeroResult();
	}

	_renderZeroResult() {
		if (this.result.length <= 0) this.selectorNothing.classList.add('show');
		else this.selectorNothing.classList.remove('show');
	}
	
	renderCards() {
		this.link = [...document.querySelectorAll('.startpage__stuff-item')];
		this.render = this.link.filter(f => f.classList.add('load'));
	}
	
	afterBuildingCards() {
		this.renderCards();
		this.linkDefaultValue = [...document.querySelectorAll('[type=button]')].map(f => f.value);
		this.defaultInputName = [...document.querySelectorAll('INPUT[type=button]')].map(f => f.name);
		this.liveSearch();
	}

	buildFilterList() {
		const selectAllFilters = [...document.querySelectorAll('input[type=button]')];
		this.categoryCleanData = selectAllFilters.map((f, index) => ({
			selectorID: selectAllFilters[index].id,
			selectorName: selectAllFilters[index].name,
			selectorValue: selectAllFilters[index].value
		}));

		const newCategory = x => new AddCategoryGenerator(x);
		this.categoryCleanData.map((f, index) => newCategory({
			selector: `#${f.selectorID}`,
			array: this.mainData,
			category: f.selectorName,
			tooltip: f.selectorValue
		}));
	}

	fetchMyData() {
		const sendRequest = JSON.stringify({ data: '' }, null, '\t');
		const options = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			mode: 'cors',
			cache: 'default',
			body: sendRequest
		};

		const originUrl = this.dataUrl;
		const noCORS = 'https://cors-anywhere.herokuapp.com/';
		const url = `${noCORS}${originUrl}`;
		// const url = `${originUrl}`;

		fetch(url, options)
			.then(response => response.json())
			.then((data) => {
				this.mainData = [...data.items];
				this.mainDataLower = this.mainData.map(f => ({ ...f, title: f.title.toLowerCase() }));

				const newCard = x => new AddCardGenerator(x);
				this.mainData.map(f => newCard({
					selector: this.selector,
					picture: f.courseId,
					title: f.subject,
					grade: f.grade,
					genre: f.genre,
					extra: `#${f.shopUrl}`,
					button: `#${f.courseId}`
				}));
			})
			.then(this.buildFilterList.bind(this))
			.then(this.afterBuildingCards.bind(this))
			.then(this.eventHandlers.bind(this))
			.then(this.successLoading.bind(this));
		// .catch(onRejected => console.error(this.errorText, `\n${onRejected.message}`));
	}

}
