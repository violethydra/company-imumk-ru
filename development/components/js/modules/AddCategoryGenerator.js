export default class AddCategoryGenerator {
	constructor(init) {
		this.selector = init.selector;
		this.arr = init.array;
		this.category = init.category;
		this.tooltip = init.tooltip || '__NULL__';
		this.myClassName = 'dropdown__item dropdown__select';
		this.selectLi = document.querySelector('.js__createDropList');
		this.createParent = () => document.createElement('UL');
		this.createChild = () => document.createElement('LI');

		this.constructor.info();
		this.categoryBuildList();
	}

	static info() { console.log('•• ADDON:', this.name, true); }

	categoryBuildList() {
		const createUL = this.createParent();
		createUL.className = this.myClassName;
		let createLI = this.createChild();
		createLI.innerHTML = this.tooltip;
		createUL.appendChild(createLI);

		const nextStep = this.category !== 'grade' ? this.calculateList() : this.calculateNumbers();
		nextStep.forEach((f) => {
			createLI = this.createChild();
			createLI.innerHTML += f;
			createUL.appendChild(createLI);
		});

		this.selectLi.querySelector(this.selector).parentElement.appendChild(createUL);
	}

	calculateList() {
		return [...new Set(this.arr.map(f => f[this.category]))].sort();
	}

	calculateNumbers() {
		const myNumber = this.calculateList().map(Number).reduce((acc, val) => {
			acc[0] = (acc[0] === undefined || val < acc[0]) ? val : acc[0];
			acc[1] = (acc[1] === undefined || val > acc[1]) ? val : acc[1];
			return acc;
		}, []);
		return this.calculateRange(...myNumber);
	}

	calculateRange(start, end) {
		this.result = [...Array(1 + end - start).keys()].map(f => f + start);
		return this.result;
	}

}
