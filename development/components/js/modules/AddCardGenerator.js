export default class AddCardGenerator {
	constructor(init) {
		this.selector = document.querySelector('.js__stuffGenerator');
		this.picture = init.picture || '__NULL__';
		this.title = init.title || '__NULL__';
		this.grade = init.grade || '__NULL__';
		this.genre = init.genre || '__NULL__';
		this.extra = init.extra || '__NULL__';
		this.button = init.button || '__NULL__';
		this.extraName = 'класс';

		this.constructor.info();
		this.buildCard();
		this._handler();
	}

	static info() { console.log('•• ADDON:', this.name, true); }
	
	gradeFix() {
		const singleNumber = x => `${x[0]} ${this.extraName}`;
		const manyNumber = x => `${Math.min(...x)}-${Math.max(...x)} ${this.extraName}ы`;
		const result = arr => (arr.length === 1 ? singleNumber(arr) : manyNumber(arr));
		const stringToNumber = arr => arr.split(';').map(Number);
		
		return result(stringToNumber(this.grade));
	}

	buildCard() { 
		const createDiv = document.createElement('DIV');
		createDiv.classList.add('startpage__stuff-item');
		createDiv.innerHTML += ` 
				<div class="stuffcard stuffcard__box">
					<div class="stuffcard__image">
						<img src="https://www.imumk.ru/svc/coursecover/${this.picture}" alt="Picture" title="Picture">
					</div>
						<div class="stuffcard__desc">
							<div class="stuffcard__subject">${this.title}</div>
							<div class="stuffcard__grade">${this.gradeFix()}</div>
							<div class="stuffcard__genre">${this.genre}</div>
							<div class="stuffcard__extra"><a class="stuffcard__href" title="URL" role="link" href="${this.extra}">Подробнее</a></div>
							<a class="stuffcard__button" title="URL" role="link" href="${this.button}">Попробовать</a>
						</div>
					</div> 
				`;

		this.selector.appendChild(createDiv);
	}
	
	whatClicked(event) {
		this._target = event.target;
		event.preventDefault();
		if (this._target.matches('A')) console.log('click :', this._target.href);
	}
	
	_handler() { 	
		this.selector.addEventListener('click', this.whatClicked, false);
	}
}
