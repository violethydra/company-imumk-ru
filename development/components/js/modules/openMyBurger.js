export default class AddOpenMyBurger {
	constructor(init) {
		this.selector = document.querySelector(init.burger);
		this.navbar = document.querySelector(init.navbar);
		this.cloneMe = document.querySelector(init.list);
		this.errorText = '💀 Ouch, database error...';
	}

	static info() { console.log('• MODULE:', this.name, true); }
	
	run() {
		if (this.selector) {
			this.constructor.info();
			this.createCopyLinks();
			this.eventHandlers();
		}
	}
	
	eventHandlers() {
		this.selector.addEventListener('click', event => this.toggleBurger(event), false);
	}
	
	createCopyLinks() {
		try {
			this.newCloneDiv = document.importNode(this.navbar, true);
			this.newCloneDiv.classList.add('myheader__list--cloned');
			this.newCloneDiv.classList.remove('myheader__list');
			this.cloneMe.after(this.newCloneDiv);
		} catch (error) {
			console.error('[ERROR] Hamburger list is empty!');
		}
	}

	toggleBurger() {
		this.selector.classList.toggle('open');
		if (window.innerWidth <= 678) this.navbar.classList.toggle('open');
		if (window.innerWidth >= 678) this.newCloneDiv.classList.toggle('open');
	}
}
