// ============================
//    Name: index.js
// ============================

import AddMoveScrollUP from './modules/moveScrollUP';
import AddOpenMyBurger from './modules/openMyBurger';
import AddServerData from './modules/AddServerData';

const start = () => {
	console.log('DOM:', 'DOMContentLoaded', true);

	new AddMoveScrollUP({
		selector: '.js__moveScrollUP',
		speed: 8
	}).run();

	new AddOpenMyBurger({
		burger: '.js__navHamburger',
		navbar: '.js__navHamburgerOpener',
		list: '.js__navHamburgerClone'
	}).run();

	new AddServerData({
		selector: '.js__serverControlPanel',
		dataUrl: 'http://krapipl.imumk.ru:8082/api/mobilev1/update'
	}).run();

};

if (typeof window !== 'undefined' && window && window.addEventListener) {
	document.addEventListener('DOMContentLoaded', start(), false);
}